package com.web.gallery.controller;

import com.web.gallery.model.File;
import com.web.gallery.service.DropboxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("dropbox")
public class DropboxController {

	private static final Logger logger = LoggerFactory.getLogger(DropboxController.class);

	@Autowired
	DropboxService dropboxService;

	@PostMapping("/upload")
	public File handleFileUplad(@RequestParam("file") MultipartFile file, @RequestParam("filePath") String filePath)
			throws Exception {
		logger.info("Uploading File to -> {}", filePath);
		return dropboxService.uploadFile(file, filePath);
	}

	@GetMapping("/list")
	public List<Map<String, Object>> index(
			@RequestParam(value = "target", required = false, defaultValue = "") String target) throws Exception {
		return dropboxService.getFileList(target);
	}

	@GetMapping("/browse")
	public Map<String, Object> brwose(
			@RequestParam(value = "target", required = false, defaultValue = "") String target) throws Exception {
		Map<String, Object> data = new HashMap<>();
		data.put("data", dropboxService.getDropboxItems(target));

		return data;
	}

	@GetMapping("/download/{fileId}")
	public void downloadFile(HttpServletResponse response, @PathVariable UUID fileId) throws Exception {
		dropboxService.downloadFile(response, fileId);
	}

	@DeleteMapping("/delete")
	public ResponseEntity<?> deleteFile(@PathVariable UUID fileId) throws Exception {
		dropboxService.deleteFile(fileId);

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

}
