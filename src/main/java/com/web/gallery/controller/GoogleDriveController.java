package com.web.gallery.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.web.gallery.model.File;
import com.web.gallery.service.GoogleDriveService;

@RestController
@RequestMapping("api/v2")
public class GoogleDriveController {

	private static final Logger logger = LoggerFactory.getLogger(GoogleDriveController.class);

	@Autowired
	GoogleDriveService googleDriveService;

	// Auth
	@GetMapping("/Callback")
	public void saveAuthorizationCode(HttpServletRequest request) throws Exception {
		googleDriveService.saveAuthorizationCode(request);
	}

	@GetMapping(value = { "/googlesignin" })
	public void doGoogleSignIn(HttpServletResponse response) throws Exception {
		googleDriveService.doGoogleSignIn(response);
	}
	// End Auth

	// Endpoints
	@GetMapping(value = { "/files" })
	public List<com.web.gallery.model.File> files() throws Exception {
		return googleDriveService.files();
	}

	@PostMapping(value = { "/createfolder/{folderName}" }, produces = "application/json")
	public String createFolder(@PathVariable(name = "folderName") String folder) throws Exception {
		return googleDriveService.createFolder(folder);
	}

	@PostMapping(value = { "/create" })
	public File createFile(MultipartFile file) throws Exception {
		return googleDriveService.createFile(file);
	}

	@PostMapping(value = { "/uploadinfolder/{name}" })
	public File uploadFileInFolder(MultipartFile file, @PathVariable String name) throws Exception {
		return googleDriveService.uploadFileInFolder(file, name);
	}

	@GetMapping(value = { "/download/{fileId}" })
	public void downloadFile(HttpServletResponse response, @PathVariable UUID fileId) throws Exception {
		googleDriveService.downloadFile(response, fileId);
	}

	@PostMapping(value = { "/makepublic/{fileId}" }, produces = { "application/json" })
	public String makePublic(@PathVariable(name = "fileId") UUID fileId) throws Exception {
		return googleDriveService.makePublic(fileId);
	}

	@DeleteMapping(value = { "/deletefile/{fileId}" }, produces = "application/json")
	public String deleteFile(@PathVariable(name = "fileId") UUID id) throws Exception {
		return googleDriveService.deleteFile(id);
	}

	@DeleteMapping(value = { "/deletefolder/{folderId}" }, produces = "application/json")
	public String deleteFolder(@PathVariable(name = "folderId") UUID id) throws Exception {
		return googleDriveService.deleteFolder(id);
	}

}
