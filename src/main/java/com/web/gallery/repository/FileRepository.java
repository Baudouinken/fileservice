package com.web.gallery.repository;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.web.gallery.model.File;

public interface FileRepository extends JpaRepository<File, UUID> {

	List<File> findByIdIn(Set<UUID> ids);
}
