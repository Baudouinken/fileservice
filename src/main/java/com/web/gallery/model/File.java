package com.web.gallery.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
public class File extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = -8288251714871303362L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	private UUID id;

	@Column(columnDefinition = "varchar")
	private String filename;

	@Column(columnDefinition = "varchar")
	private String remoteId;

	private Long filesize;

	@Column(columnDefinition = "varchar")
	private String path;

	@Column(columnDefinition = "varchar")
	private String fileLink;

	@Column(columnDefinition = "varchar")
	private String contentType;

	@Transient
	private boolean isPublic = true;

	@Transient
	private Object linkShareMetadata;

	public File() {

	}

	public Object getLinkShareMetadata() {
		return linkShareMetadata;
	}

	public void setLinkShareMetadata(Object linkShareMetadata) {
		this.linkShareMetadata = linkShareMetadata;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Long getFilesize() {
		return filesize;
	}

	public void setFilesize(Long filesize) {
		this.filesize = filesize;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getRemoteId() {
		return remoteId;
	}

	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	public String getFileLink() {
		return fileLink;
	}

	public void setFileLink(String fileLink) {
		this.fileLink = fileLink;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public boolean isPublic() {
		return isPublic;
	}

	public void setPublic(boolean aPublic) {
		isPublic = aPublic;
	}
}
