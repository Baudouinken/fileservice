# Fileservice



## Getting started

It is an api in which is implemented the different Apis of Dropbox and GoogleDrive that allow me to manage the files for my different applications.

![Dropbox-API](https://www.clevercomponents.com/images/dropbox-large.jpg)


![Google-Drive-API](https://i0.wp.com/technicalsand.com/wp-content/uploads/2020/11/Google-Drive-API-integration-with-Spring-boot-min.png?fit=1023%2C575&ssl=1)
