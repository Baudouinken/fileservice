package com.web.gallery.service;

import com.dropbox.core.BadRequestException;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.dropbox.core.v2.files.WriteMode;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.gallery.model.DropboxItem;
import com.web.gallery.model.File;
import com.web.gallery.repository.FileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.*;

@Service
public class DropboxService {

	private static final Logger logger = LoggerFactory.getLogger(DropboxService.class);

//	private static final List<String> SUPPORTED_IMAGE_TYPES = Arrays.asList("JPEG", "BMP", "JPEG", "WBMP", "PNG",
//			"GIF");

	@Autowired
	private FileRepository fileRepo;

	@Autowired
	private DbxClientV2 dropboxClient;

	public File uploadFile(MultipartFile file, String filePath) throws IOException, DbxException {

		ByteArrayInputStream inputStream = new ByteArrayInputStream(file.getBytes());

		File newFile = new File();
		FileMetadata metadata = null;
		try (InputStream initialStream = file.getInputStream()) {

			// upload file
			metadata = dropboxClient.files().uploadBuilder(filePath + "/" + file.getOriginalFilename())
					.withMode(WriteMode.ADD).uploadAndFinish(initialStream);

			logger.info("Uploaded file '{}' to '{}' ", file.getOriginalFilename(),
					filePath + file.getOriginalFilename());
		} catch (Exception e) {
			logger.error("Error during file upload !! -> {}", e.getMessage());
			throw new BadRequestException("Error", "Error during file upload !!");
		}

		// get the link
		String link = dropboxClient.sharing().createSharedLinkWithSettings(filePath + "/" + file.getOriginalFilename())
				.getUrl();

		String[] result = link.split("\\?");

		// change the end of the link from dl=0 to raw=1
		link = result[0] + "?raw=1";

		newFile.setFilename(file.getOriginalFilename());
		newFile.setPath(filePath + "/" + file.getOriginalFilename());
		newFile.setFilesize(metadata.getSize());
		newFile.setRemoteId(metadata.getId());
		newFile.setFileLink(link);
		newFile.setContentType(file.getContentType());

		fileRepo.save(newFile);

		inputStream.close();

		return newFile;
	}

	public List<Map<String, Object>> getFileList(String target) throws IOException, DbxException {
		List<Metadata> entries = dropboxClient.files().listFolder(target).getEntries();
		List<Map<String, Object>> result = new ArrayList<>();

		for (Metadata entry : entries) {
			if (entry instanceof FileMetadata) {
				logger.info("{} is file", entry.getName());
			}
			String metaDataString = entry.toString();
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = new HashMap<>();
			map = mapper.readValue(metaDataString, new TypeReference<Map<String, Object>>() {
			});
			result.add(map);
		}

		return result;
	}

	public List<DropboxItem> getDropboxItems(String path) throws IOException, DbxException {
		List<Metadata> entries = dropboxClient.files().listFolder(path).getEntries();
		List<DropboxItem> result = new ArrayList<>();
		entries.stream().forEach(entry -> {
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = new HashMap<>();
			try {
				map = mapper.readValue(entry.toString(), new TypeReference<Map<String, Object>>() {
				});
			} catch (IOException e) {
				e.printStackTrace();
			}

			DropboxItem item = new DropboxItem();
			item.setId(map.get("id").toString());
			item.setName(map.get("name").toString());
			item.setPath(map.get("path_lower").toString());
			result.add(item);
		});
		return result;
	}

	public void downloadFile(HttpServletResponse response, UUID id) throws IOException, DbxException {

		File file = findFile(id);

		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		response.setHeader("Content-Disposition",
				"attachment; filename=\"" + URLEncoder.encode(file.getFilename(), "UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");

		ServletOutputStream outputStream = response.getOutputStream();
		dropboxClient.files().downloadBuilder(file.getPath()).download(outputStream);

		response.getOutputStream().flush();
		response.getOutputStream().close();
	}

	public void deleteFile(UUID id) throws DbxException {

		File file = findFile(id);

		dropboxClient.files().delete(file.getPath());

		fileRepo.deleteById(file.getId());
	}

	private File findFile(UUID id) throws BadRequestException {
		return fileRepo.findById(id)
				.orElseThrow(() -> new BadRequestException("", "Cannot find Helper with id: " + id));
	}

}
