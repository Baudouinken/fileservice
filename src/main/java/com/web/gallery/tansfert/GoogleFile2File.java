package com.web.gallery.tansfert;

import java.util.List;
import java.util.stream.Collectors;

import com.web.gallery.model.File;

public class GoogleFile2File {
	
	public static File apply(com.google.api.services.drive.model.File in) {
		
		File out = new File();
		out.setFilename(in.getName());
		out.setRemoteId(in.getId());
		out.setFilesize(in.getSize());
		out.setFileLink(in.getWebContentLink());	
		out.setContentType(in.getMimeType());
		out.setLinkShareMetadata(in.getLinkShareMetadata());
		
		return out;		
	}
	
	public static List<File> apply(List<com.google.api.services.drive.model.File> in) {
		return in.stream().map(e -> apply(e)).collect(Collectors.toList());
	}

}
