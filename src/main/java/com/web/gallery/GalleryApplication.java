package com.web.gallery;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.dropbox.core.DbxException;
import com.web.gallery.config.SpringSecurityAuditorAware;

@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@SpringBootApplication
public class GalleryApplication {

	public static void main(String[] args) throws IOException, DbxException {
		SpringApplication.run(GalleryApplication.class, args);

		// logic used to generate access and refresh token

//        DbxRequestConfig config = DbxRequestConfig.newBuilder("dropbox/").build();
//
//        String dropboxAppKey = "8bhx159frwf9bgm"; // Put your Dropbox App Key here
//        String dropboxAppSecret = "volzkwzbe0dnql9"; // Put your Dropbox App Secret here
//
//        DbxAppInfo appInfo = new DbxAppInfo(dropboxAppKey, dropboxAppSecret);
//
//        DbxAuthFinish authFinish = scopeAuthorize(appInfo, config);
//
//        System.out.println("Authorization complete.");
//        System.out.println("- User ID: " + authFinish.getUserId());
//        System.out.println("- Account ID: " + authFinish.getAccountId());
//        System.out.println("- Access Token: " + authFinish.getAccessToken());
//        System.out.println("- Expires At: " + authFinish.getExpiresAt());
//        System.out.println("- Refresh Token: " + authFinish.getRefreshToken());
//        System.out.println("- Scope: " + authFinish.getScope());
	}

//	private static DbxAuthFinish scopeAuthorize(DbxAppInfo appInfo, DbxRequestConfig requestConfig) throws IOException, DbxException {
//        DbxWebAuth webAuth = new DbxWebAuth(requestConfig, appInfo);
//
//        DbxWebAuth.Request webAuthRequest = DbxWebAuth.newRequestBuilder()
//                .withNoRedirect()
//                .withTokenAccessType(TokenAccessType.OFFLINE)
//                // Define here the scopes that you need in your application - and the app created in Dropbox has
//                .withScope(Arrays.asList("files.content.read", "files.content.write", "sharing.write", "file_requests.write"))
//                .withIncludeGrantedScopes(IncludeGrantedScopes.USER)
//                .build();
//
//        String authorizeUrl = webAuth.authorize(webAuthRequest);
//        System.out.println("1. Go to " + authorizeUrl);
//        System.out.println("2. Click \"Allow\" (you might have to log in first).");
//        System.out.println("3. Copy the authorization code.");
//        System.out.print("Enter the authorization code here: ");
//
//        String code = new BufferedReader(new InputStreamReader(System.in)).readLine();
//        if (code == null) {
//            System.exit(1);
//        }
//        code = code.trim();
//
//        DbxAuthFinish authFinish = webAuth.finishFromCode(code);
//
//        System.out.println("Successfully requested scope " + authFinish.getScope());
//        return authFinish;
//    }
//
//	// first used before switched to permanant to refresh token
//	@SuppressWarnings("deprecation")
//	@Bean("dropboxClient")
//	public DbxClientV2 dropboxClient() throws DbxException {
//		String ACCESS_TOKEN = "sl.BK4JdW6PQeqJzblWBMKE7ufQgCbSdUXsPRxrSwGClt-xjiDUSEa72MLfGZ0RZwHxdOwomoQ9Ym65Nf-9vYOUNF1rrD07jOvbL7gL2DjeG9nORA8XmP_--8PSS7QbSYOmGi8eAitaIBYe";
//		DbxRequestConfig config = new DbxRequestConfig("dropbox/truk", "en_US");
//		DbxClientV2 client = new DbxClientV2(config, ACCESS_TOKEN);
//		return client;
//	}

	// Auditing Config
	@Bean
	public AuditorAware<String> auditorAware() {
		return new SpringSecurityAuditorAware();
	}
}
