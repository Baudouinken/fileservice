package com.web.gallery.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.dropbox.core.BadRequestException;
import com.dropbox.core.DbxException;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeRequestUrl;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.web.gallery.repository.FileRepository;
import com.web.gallery.tansfert.GoogleFile2File;
import com.web.gallery.util.MultipartUtils;

import javassist.NotFoundException;

@Service
public class GoogleDriveService {

	@Autowired
	private FileRepository fileRepo;

	/**
	 * Application name.
	 */
	private static final String APPLICATION_NAME = "File Storage";
	/**
	 * Global instance of the JSON factory.
	 */
	private static final JsonFactory JSON_FACTORY = GsonFactory.getDefaultInstance();
	/**
	 * Directory to store authorization tokens for this application.
	 */
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	private GoogleAuthorizationCodeFlow flow;

	private Drive service;

	private Credential credential;

	private static final String CREDENTIALS_FILE_PATH = "/code_secret_auth0.json";

	@PostConstruct
	public void init() throws Exception {
		InputStream in = GoogleDriveService.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		if (in == null) {
			throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		// Build flow and trigger user authorization request.
		flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets,
				DriveScopes.all()).setDataStoreFactory(
				new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH))).setAccessType("offline").build();

		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8766).build();
		credential = new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");

		service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME)
				.build();
	}

	public void saveAuthorizationCode(HttpServletRequest request) throws Exception {
		String code = request.getParameter("code");
		if (code != null) {
			saveToken(code);
		}
	}

	public void doGoogleSignIn(HttpServletResponse response) throws Exception {
		GoogleAuthorizationCodeRequestUrl url = flow.newAuthorizationUrl();
		String redirectURL = url.setRedirectUri("https://85.214.194.89:8766/Callback").setAccessType("offline").build();
		response.sendRedirect(redirectURL);
	}

	private void saveToken(String code) throws Exception {
		GoogleTokenResponse response = flow.newTokenRequest(code).setRedirectUri("https://85.214.194.89:8766/Callback")
				.execute();
		flow.createAndStoreCredential(response, "MY_DUMMY_USER");
	}

	public List<com.web.gallery.model.File> files() throws Exception {
		FileList result = service.files().list()
				// .setPageSize(10)
				.setFields("files(id, name, createdTime, size, webContentLink, mimeType, linkShareMetadata)").execute();
		List<File> files = result.getFiles();
		if (files == null || files.isEmpty()) {
			throw new NotFoundException("No files found.");
		} else {
			return GoogleFile2File.apply(files);
		}
	}

	public String createFolder(String folder) throws Exception {

		File file = new File();
		file.setName(folder);
		file.setMimeType("application/vnd.google-apps.folder");

		File created = service.files().create(file).setFields("id").execute();

		return "Folder has been created successfully -> ID: " + created.getId();
	}

	public com.web.gallery.model.File createFile(MultipartFile file) throws Exception {
		
		FileList getFolder = service.files().list()
				.setQ("mimeType='application/vnd.google-apps.folder' and name = 'api'")
				.setFields("files(id, name)").execute();
		List<File> files = getFolder.getFiles();

		java.io.File file_ = MultipartUtils.multipartToFile( file, "test");

		File newFile = new File();
		newFile.setName(UUID.randomUUID().toString() + file.getOriginalFilename());
		newFile.setParents(Arrays.asList(files.get(0).getId()));

		FileContent content = new FileContent(file.getContentType(), file_);
		File uploadedFile = service.files().create(newFile, content)
				.setFields("id, thumbnailLink, size, webViewLink")
				.execute();

		com.web.gallery.model.File newFileObj = new com.web.gallery.model.File();
		newFileObj.setFilename(UUID.randomUUID().toString() + file.getOriginalFilename());
		newFileObj.setPath(uploadedFile.getThumbnailLink());
		newFileObj.setFilesize(uploadedFile.getSize());
		newFileObj.setRemoteId(uploadedFile.getId());
		newFileObj.setFileLink(uploadedFile.getWebViewLink());
		newFileObj.setContentType(file.getContentType());

		newFileObj =fileRepo.save(newFileObj);

		return newFileObj;
	}

	public com.web.gallery.model.File uploadFileInFolder(MultipartFile file, String name) throws Exception {

		FileList getFolder = service.files().list()
				.setQ("mimeType='application/vnd.google-apps.folder' and name = '" + name + "'")
				.setFields("files(id, name)").execute();
		List<File> files = getFolder.getFiles();

		java.io.File file_ = file.getResource().getFile();

		File newFile = new File();
		newFile.setName(UUID.randomUUID().toString() + file.getOriginalFilename());
		newFile.setParents(Arrays.asList(files.get(0).getId()));

		FileContent content = new FileContent(file.getContentType(), file_);
		File uploadedFile = service.files().create(newFile, content).setFields("id").execute();

		com.web.gallery.model.File newFileObj = new com.web.gallery.model.File();
		newFileObj.setFilename(UUID.randomUUID().toString() + file.getOriginalFilename());
		newFileObj.setPath(uploadedFile.getThumbnailLink());
		newFileObj.setFilesize(uploadedFile.getSize());
		newFileObj.setRemoteId(uploadedFile.getId());
		newFileObj.setFileLink(uploadedFile.getWebViewLink());
		newFileObj.setContentType(file.getContentType());

		newFileObj = fileRepo.save(newFileObj);
		return newFileObj;
	}

	public void downloadFile(HttpServletResponse response, UUID id) throws IOException, DbxException {

		com.web.gallery.model.File file = findFile(id);

		response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
		response.setHeader("Content-Disposition",
				"attachment; filename=\"" + URLEncoder.encode(file.getFilename(), "UTF-8") + "\";");
		response.setHeader("Content-Transfer-Encoding", "binary");

		ServletOutputStream outputStream = response.getOutputStream();
		service.files().get(file.getRemoteId())
				.executeMediaAndDownloadTo(outputStream);

		response.getOutputStream().flush();
		response.getOutputStream().close();
	}

	public String makePublic(UUID id) throws Exception {

		com.web.gallery.model.File file = findFile(id);

		Permission permission = new Permission();
		permission.setType("anyone");
		permission.setRole("reader");

		service.permissions().create(file.getRemoteId(), permission).execute();

		return "Permission has been successfully granted.";
	}

	public String deleteFile(UUID id) throws Exception {
		com.web.gallery.model.File file = findFile(id);

		FileList result = service.files().list().setQ("mimeType = 'application/vnd.google-apps.folder' and name = '" + file.getFilename() + "'").setFields("files(id, name)").execute();
		List<File> files = result.getFiles();

		service.files().delete(files.get(0).getId()).execute();

		fileRepo.deleteById(file.getId());
		return "File has been deleted.";
	}

	public String deleteFolder(UUID id) throws Exception {
		com.web.gallery.model.File file = findFile(id);

		FileList result = service.files().list().setQ("mimeType != 'application/vnd.google-apps.folder' and name = '" + file.getFilename() + "'").setFields("files(id, name)").execute();
		List<File> files = result.getFiles();

		service.files().delete(files.get(0).getId()).execute();

		fileRepo.deleteById(file.getId());
		return "File has been deleted.";
	}

	private com.web.gallery.model.File findFile(UUID id) throws BadRequestException {
		return fileRepo.findById(id)
				.orElseThrow(() -> new BadRequestException("", "Cannot find Helper with id: " + id));
	}

}